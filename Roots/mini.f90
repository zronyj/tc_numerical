! -----------------------------------------------------------------------------------------
! This function should be defined by the user of the program
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - x: a real number
!
! Outputs:
! - funcion: a real number
!
! -----------------------------------------------------------------------------------------
real(kind=8) function g(x)
	real(kind=8) :: x
	g = exp(-x) * (3.2 * sin(x) - 0.5 * cos(x))
	return
end function

program mini

! -----------------------------------------------------------------------------------------
! This program computes the foots of a given function using 2 methods:
! - Bisection method
! - Regula Falsi method
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - funcion: A function defined at the beginning of this program. It is imported as an
!            external function.
!
! Outputs:
! - on screen: It shows the value of the roots of the function in a given interval.
!              This is done as a homework example.
!
! -----------------------------------------------------------------------------------------

    ! Import the integration modules ------------------------------------------------------
	use minima, only : bisection, regulafalsi

	implicit none

	! Import the function defined at the beginning of the file ----------------------------
	real(kind=8), external :: g
	! Variables ---------------------------------------------------------------------------
	real(kind=8) :: a, b
	real(kind=8), dimension(2) :: bisec, regulaf

	! Interval limits ---------------------------------------------------------------------
	a = 3.0
	b = 4.0

	write(*, *) "----------------------------------------------------------"
    write(*, *) "|                      Root Finder                       |"
    write(*, *) "----------------------------------------------------------"
    write(*, *) "|                                                        |"
    write(*, *) "|  This program will compute the roots of the following  |"
    write(*, *) "|  function, using the Bisection and Regula Falsi        |"
    write(*, *) "|  methods.                                              |"
    write(*, *) "|                                                        |"
    write(*, *) "|             (-x)                                       |"
    write(*, *) "|     g(x) = e     . (3.2 . sin(x) - 0.5 . cos(x))       |"
    write(*, *) "|                                                        |"
    write(*, *) "|  It will start attempting to find the root in the      |"
    write(*, *) "|  region from a = 3 to b = 4.                           |"
    write(*, *) "|                                                        |"

	if (g(a) .eq. 0) then
		write(*, *) "|  There is no need to look for the root, since g(a) is  |"
		write(*, *) "|  already a root.                                       |"
		write(*, *) "|                                                        |"
		write(*, *) "|                                                        |"
	    write(*, *) "|                 Thank you for using Root Finder!       |"
	    write(*, *) "|                                                        |"
	    write(*, *) "----------------------------------------------------------"
	    stop
	else if (g(b) .eq. 0) then
		write(*, *) "|  There is no need to look for the root, since g(b) is  |"
		write(*, *) "|  already a root.                                       |"
		write(*, *) "|                                                        |"
		write(*, *) "|                                                        |"
	    write(*, *) "|                 Thank you for using Root Finder!       |"
	    write(*, *) "|                                                        |"
	    write(*, *) "----------------------------------------------------------"
	    stop
	end if

	do while (g(a)/g(b) > 0)
		write(*, *) "|  WARNING!                                              |"
		write(*, *) "|                                                        |"
		write(*, *) "|  The values of a and b are not comprising a region in  |"
		write(*, *) "|  which the function has a root! Please check for other |"
		write(*, *) "|  values of a and b.                                    |"
		write(*, *) "|                                                        |"
		write(*, *) "----------------------------------------------------------"
		write(*, *) "|                                                        |"
		write(*, "(' ', A)", advance='no') "|  Please enter a new value for (a): "
		read(*, *) a
		write(*, "(' ', A)", advance='no') "|  Please enter a new value for (b): "
		read(*, *) b
		write(*, *) "|                                                        |"
		write(*, *) "----------------------------------------------------------"
	end do

	write(*, *) "|                                                        |"
    write(*, *) "|                 Computing the roots of g(x) ...        |"
    write(*, *) "|                                                        |"
    write(*, *) "----------------------------------------------------------"

    ! Computing the root, using the bisection method --------------------------------------
    call bisection(g, a, b, bisec)

    write(*, *) "|                                                        |"
    write(*, *) "|  The Bisection method found a minimum at               |"
    write(*, "(' ', A6, F16.8, A36)") "|  x =", bisec(1), "|"
    write(*, "(' ', A23, I6, A29)") "|  and it required n = ", int(bisec(2)), " iterations.                |"

    ! Computing the root, using the regula falsi method -----------------------------------
    call regulafalsi(g, a, b, regulaf)

    write(*, *) "|                                                        |"
    write(*, *) "|  The Regula Falsi method found a minimum at            |"
    write(*, "(' ', A6, F16.8, A36)") "|  x =", regulaf(1), "|"
    write(*, "(' ', A23, I6, A29)") "|  and it required n = ", int(regulaf(2)), " iterations.                |"

    write(*, *) "|                                                        |"
    write(*, *) "|                 Thank you for using Root Finder!       |"
    write(*, *) "|                                                        |"
    write(*, *) "----------------------------------------------------------"


end program mini