module minima

	implicit none

	! Establishing the subroutines that will be provided to the external program
	public :: bisection, regulafalsi

	contains

	subroutine bisection(f, a, b, bisec)
	! -------------------------------------------------------------------------------------
    ! Special subroutine to calculate the roots of a function using the Bisection method
    ! -------------------------------------------------------------------------------------
    !
    ! Inputs
    ! - f: the function whose roots we want to find
    ! - a: the lower limit of the interval
    ! - b: the upper limit of the interval
    ! 
    ! Outputs
    ! - bisec: the root of the function and the number of iterations
    ! 
    ! -------------------------------------------------------------------------------------

		implicit none

		! Internal variables --------------------------------------------------------------
		integer :: n
		real(kind=8) :: f, c, t
		real(kind=8), dimension(2) :: bisec

		! In and out variables ------------------------------------------------------------
		real(kind=8), intent(inout) :: a, b

		! Setting the initial values for the iterations and the slope's direction ---------
		n = 0
		c = 1.0
		t = f(b) - f(a)
		t = t / abs(t)

		! Computing the next point by finding a middle point between a and b, and checking
		! if it's before or after the root, so that the interval can be re-defined --------
		do while (abs(f(c)) .gt. 10D-9)
			c = a + (b - a) / 2
			if (t * f(c) .lt. 0) then
				a = c
			else if (t * f(c) .gt. 0) then
				b = c
			else
				exit
			end if
			n = n + 1
		end do

		bisec = (/ c, real(n, 8) /)

	end subroutine bisection

	subroutine regulafalsi(f, a, b, regulaf)
	! -------------------------------------------------------------------------------------
    ! Special subroutine to calculate the roots of a function using the Regula Falsi method
    ! -------------------------------------------------------------------------------------
    !
    ! Inputs
    ! - f: the function whose roots we want to find
    ! - a: the lower limit of the interval
    ! - b: the upper limit of the interval
    ! 
    ! Outputs
    ! - regulaf: the root of the function and the number of iterations
    ! 
    ! -------------------------------------------------------------------------------------

		implicit none

		! Internal variables --------------------------------------------------------------
		integer :: n
		real(kind=8) :: f, c, t
		real(kind=8), dimension(2) :: regulaf

		! In and out variables ------------------------------------------------------------
		real(kind=8), intent(inout) :: a, b

		! Setting the initial values for the iterations and the slope's direction ---------
		n = 0
		c = 1.0
		t = f(b) - f(a)
		t = t / abs(t)

		! Computing the next point by finding the root of a straight line from a to b and,
		! if the line's root is before or after the function's root, so that the interval -
		! can be re-defined ---------------------------------------------------------------
		do while (abs(f(c)) .gt. 10D-9)
			c = (b * f(a) - a * f(b)) / (f(a) - f(b))
			if (t * f(c) .lt. 0) then
				a = c
			else if (t * f(c) .gt. 0) then
				b = c
			else
				exit
			end if
			n = n + 1
		end do

		regulaf = (/ c, real(n, 8) /)

	end subroutine regulafalsi

end module minima