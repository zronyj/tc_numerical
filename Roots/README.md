
# Roots Exercise 1

Last revision: Dec 15, 2021

---

This program solves the third homework exercise of the numerical section in the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the general README outside of the folder of this program. It should provide details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a standalone, please check sections 1. a), 1. b) and 1. c) of the general README, before running the following commands:

`gfortran -c minima.f90 -o minima.o`\
`gfortran -c mini.f90 -o mini.o`\
`gfortran mini.o minima.o -o Mini.exe`

Once you have run this command successfully, you should be able to run the program.

### a) Test

The only way to test if this program is working is to actually run it. Therefore, I suggest that you try running the following command:

`./Mini.exe`

The program will greet you and tell you the interval to be used by the 2 methods to find the desired root of the following function. If the numbers for the interval don't comprise a region with a change of sign in the function, then the function doesn't have a root in there and you will be required to enter new numbers for the interval.

If the program ran successfully, then you should see the value of the froot calculated with 2 numerical methods: Bisection Method and Regula Falsi Method.

```math
g \left( x \right) = e^{-x} \left( 3.2 \sin{\left( x \right)} - 0.5 \cos{\left( x \right)} \right)
```

The root should be a real number close to $`3.296589`$ for each method.

If this is true, then the program has run successfully.

### b) Inputs

This program doesn't require any inputs.

### c) Outputs

If you run the program as described in section 1. a) of this README, then the output will be displayed on screen.

### d) Theory behind it

Finding the roots of a function can be done numerically in many ways, if the function has a root. In this case, two similar methods will be used:

#### 1. The Bisection Method

This method checks whether there is a change of sign between a given interval $`\left[a, b\right]`$ and, if true, it proceeds to compute a point $`c`$ in the middle of this interval:

```math
c = a + \frac{b - a}{2}
```

Afterwards, it checks whether the function evaluated in this point is less than, or greater than 0, to proceed and replace $`a`$ or $`b`$ with $`c`$. This will, evidently, depend of the function's slope in that interval.

#### 2. The Regula Falsi Method

This method checks whether there is a change of sign between a given interval $`\left[a, b\right]`$ and, if true, it creates a straight line between $`f \left( a \right)`$ and $`f \left( b \right)`$. The root of that straight line is considered as a new point $`c`$:

```math
c = \frac{b f \left( a \right) - a f \left( b \right)}{ f \left( a \right) - f \left( b \right) }
```

Afterwards, it checks whether the function evaluated in point $`c`$ is less than, or greater than 0, to proceed and replace $`a`$ or $`b`$ with $`c`$. This will, evidently, depend of the function's slope in that interval.

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin or enable the WSL environment, you should be able to compile and run the program. However, I do suggest that you have a look at section 1. a) of the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file, please follow this link to locate the project's repository: [https://gitlab.com/zronyj/tc_numerical](https://gitlab.com/zronyj/tc_numerical)