
# Optimization Exercise 2

Last revision: Dec 15, 2021

---

This program solves the fourth homework exercise of the numerical section in the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the general README outside of the folder of this program. It should provide details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a standalone, please check sections 1. a), 1. b) and 1. c) of the general README, before running the following commands:

`gfortran -c optimization.f90 -o optimization.o`\
`gfortran -c NewtonRaphson.f90 -o NewtonRaphson.o`\
`gfortran NewtonRaphson.o optimization.o -o NewtonRaphson.exe`

Once you have run this command successfully, you should be able to run the program.

### a) Test

The only way to test if this program is working is to actually run it. Therefore, I suggest that you try running the following command:

`./NewtonRaphson.exe`

The program will greet you and tell you the first point to be used as an approximation by the Newton-Raphson method to find the desired minimum of the following function.

```math
g \left( x \right) = x^{2} - x
```

If the program ran successfully, then you should see the value of the number of the current iteration, the value of x which makes the function a minimum, the value of the function at x, and the value of the derivative for each step.

The minimum should be a real number close to $`0.5`$.

If this is true, then the program has run successfully.

### b) Inputs

This program doesn't require any inputs.

### c) Outputs

If you run the program as described in section 1. a) of this README, then the output will be displayed on screen.

### d) Theory behind it

Finding the minimum of a function can be done numerically using the Newton-Raphson method. However, there are seveeral ways to find it. The one used by this program requires the calculation of the first and second derivatives of the function to fulfil:

```math
x_{n + 1} = x_{n} - \dfrac{f' \left( x \right)}{f'' \left( x \right)}
```

The derivatives can be approximaetd using finite numbers. The first derivative, by definition is the following:

```math
f' \left( x \right) = \frac{ f \left( x + \Delta h \right) - f \left( x \right)}{\Delta h}
```

However, since $`\Delta h`$ will be considered finite, the previous definition of a derivative is biased to one side. A better approach would be to approximate the derivative to a central point, by defining it in the following way:

```math
f' \left( x \right) = \frac{ f \left( x + \Delta h \right) - f \left( x - \Delta h \right)}{2 \Delta h}
```

Following the same logic, the second derivative can be defined in a simmilar manner for the finite case:

```math
f'' \left( x \right) = \frac{f'\left( x + \Delta h \right) - f'\left( x - \Delta h \right)}{2 \Delta h}
```

However, taking advantage of the different ways of defining a derivative, this other approach is preferred, because it uses the function evaluated at $`x`$, relying less on the approximations:

```math
f'' \left( x \right) = \frac{\frac{ f \left( x + \Delta h \right) - f \left( x \right)}{\Delta h} - \frac{ f \left( x \right) - f \left( x - \Delta h \right)}{\Delta h}}{\Delta h} = \frac{ f \left( x + \Delta h \right) - 2 f \left( x \right) + f \left( x - \Delta h \right) }{\left( \Delta  h \right)^{2}}
```

In the case of this program, $`\Delta h = 0.1`$ and $`x`$ was chosen to be 3.

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin or enable the WSL environment, you should be able to compile and run the program. However, I do suggest that you have a look at section 1. a) of the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file, please follow this link to locate the project's repository: [https://gitlab.com/zronyj/tc_numerical](https://gitlab.com/zronyj/tc_numerical)