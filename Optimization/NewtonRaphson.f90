! -----------------------------------------------------------------------------------------
! This function should be defined by the user of the program
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - x: a real number
!
! Outputs:
! - funcion: a real number
!
! -----------------------------------------------------------------------------------------
real(kind=8) function g(x)
	real(kind=8) :: x
	g = x**2 - x
	return
end function

program NewtonRaphson

! -----------------------------------------------------------------------------------------
! This program computes the minimum of a given function using the Newton-Raphson method
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - funcion: A function defined at the beginning of this program. It is imported as an
!            external function.
!
! Outputs:
! - on screen: It shows the value of x, f(x) and f'(x) at every iteration.
!              This is done as a homework example.
!
! -----------------------------------------------------------------------------------------

	! Import the integration modules ------------------------------------------------------
	use optimization, only : NR

	implicit none

	! Import the function defined at the beginning of the file ----------------------------
	real(kind=8), external :: g

	! Variables ---------------------------------------------------------------------------
	real(kind=8) :: x, h, opt

	! First point to evaluate and delta for finite derivatives ----------------------------
	x = 3.0
	h = 0.1

	write(*, *) "----------------------------------------------------------"
	write(*, *) "|                       Optimizer                        |"
	write(*, *) "----------------------------------------------------------"
	write(*, *) "|                                                        |"
	write(*, *) "|  This program will compute the following function's    |"
	write(*, *) "|  minimum using the Newton-Raphson Method.              |"
	write(*, *) "|                                                        |"
	write(*, *) "|                             2                          |"
	write(*, *) "|                     g(x) = x  - x                      |"
	write(*, *) "|                                                        |"
	write(*, *) "|  It will start attempting to find the minumim around   |"
	write(*, *) "|  the point x = 3.                                      |"
	write(*, *) "|                                                        |"
	write(*, *) "|                 Computing the minimum of g(x) ...      |"
	write(*, *) "|                                                        |"

	! Computing the minimum, using the Newton-Raphson method ------------------------------
	call NR(g, x, h, opt)

	write(*, *) "|                                                        |"
    write(*, *) "|                                                        |"
    write(*, *) "|                 Thank you for using Optimizer!         |"
    write(*, *) "|                                                        |"
    write(*, *) "----------------------------------------------------------"

end program NewtonRaphson