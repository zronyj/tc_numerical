module optimization

	implicit none

	! Establishing the subroutines that will be provided to the external program
	public :: NR

	contains

	subroutine NR(f, x, h, opt)
	! -------------------------------------------------------------------------------------
    ! Special subroutine to calculate the minimum of a function using Newton-Raphson
    ! -------------------------------------------------------------------------------------
    !
    ! Inputs
    ! - f: the function whose minimum we want to find
    ! - x: the initial value to check for the minimum
    ! - h: the delta to compute the finite derivatives
    ! 
    ! Outputs
    ! - opt: the minimum of the function
    ! 
    ! -------------------------------------------------------------------------------------

		implicit none

		! Internal variables --------------------------------------------------------------
		real(kind=8) :: f, fd, sd
		integer :: n

		! In and out variables ------------------------------------------------------------
		real(kind=8), intent(in) :: h
		real(kind=8), intent(inout) :: x
		real(kind=8), intent(out) :: opt

		! Setting the initial values for the iterations and computing the first derivative
		n = 0
		fd = (f(x + h) - f(x - h))/ (2 * h)

		do while (abs(fd) > 10D-9)

			! Computing the first and second derivatives
			! Press W, Teukolsky S, Vetterling W, Flannery B. *Numerical Recipes*. 3rd ed. 2007. pp. 229
			fd = (f(x + h) - f(x - h))/ (2 * h)
			sd = (f(x + h) - 2 * f(x) + f(x - h)) / h**2

			! Newton-Raphson method for minima
			x = x - fd/sd

			n = n + 1

			! Output of the program
			write(*, *) "----------------------------------------------------------"
			write(*, *) "|                                                        |"
			write(*, "(' ', A33, I4, A21)") "|  Computed values for iteration ", n, "|"
			write(*, *) "|                                                        |"
			write(*, "(' ', A21, F12.8, A25)") "|  The value of x is ", x, "  |"
			write(*, "(' ', A24, F12.8, A22)") "|  The value of f(x) is ", f(x), "  |"
			write(*, "(' ', A40, F12.8, A6)") "|  The value of f'(x) -the gradient- is ", fd, " |"
			write(*, *) "|                                                        |"

		end do

		write(*, *) "----------------------------------------------------------"
		opt = x

	end subroutine NR

end module optimization