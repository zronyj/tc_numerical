
# Numerical Computing

Last revision: Dec 08, 2021

---

These programs were homework exercises for the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Installation

### a) Required software

These programs have been compiled and tested using **GFortran**. Therefore, we suggest that you compile the source code using GFortran in a unix-like environment. If you don't have such an environment, please have a look at the [Installing GFortran](https://fortran-lang.org/learn/os_setup/install_gfortran) article in the Fortran official page.

To check if you have GFortran installed in your unix-like environment, please run the following command in a Terminal:

`which gfortran`

If the result isn't a path, please install GFortran using one of the following:

- Debian-based Linux: `sudo apt-get install gfortran`
- RedHat-based Linux: `sudo yum install gcc-gfortran` or `sudo dnf install gcc-gfortran`
- Arch Linux: `sudo pacman -S gcc-fortran`
- macOS: (if you have XCode installed) `xcode-select --install` otherwise `port search gcc && sudo port install gcc10`

### b) Source code

The source code for this program should be included with this README, and it should be comprised of the following:

- **Integration1**\
  |- Integral.f90\
  |- imethods.f90

- **Integration2**\
  |- MonteCarlo.f90

- **Roots**\
  |- mini.f90\
  |- minima.f90

- **Optimization**\
  |- NewtonRaphson.f90\
  |- optimization.f90

The additional files and directories are used to compile the program, explain how to install and run the program.

- **Integration1**\
  |- README.md\
  |- makefile

- **Integration2**\
  |- README.md\
  |- makefile

- **Roots**\
  |- README.md\
  |- makefile

- **Optimization**\
  |- README.md\
  |- makefile

### c) GFortran and Make versions

Before compiling the program, it would be a good idea to check the version of GFortran and Make. This program was compiled and tested using GFortran 9.3.0 and GNU Make 4.2.1. To check your GFortran and Make versions, please run the following commands in a Terminal.

`gfortran --version`

`make --version`

### d) Installation

Finally, if you have GFortran installed in your system, and the version is comparable, then please open a Terminal window in the folder with the specific exercise and run the following command:

`make`

Please note that the only files in that folder should be the ones listed in section 1. b) of this README. Otherwise, *make* may fail to compile the program.

## 2. Running each program

Each program is a very small exercise, but it requires to be executed separately. Therefore, to avoid any confusion, every program has a separate README file in its own folder.

For more information on how to run each program, please refer to that file.

## 3. Acknowledgements

Special thanks go to *Ania Beatriz Rodriguez*, *David Varas*, *Lucia Lopez Pacios*, *Marlene Bosquez* and *Orla Mary Gleeson* for the motivation and the insight.

## 4. License

Copyright 2021 Rony J. Letona

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.