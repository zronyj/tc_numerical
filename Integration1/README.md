
# Integration Exercise 1

Last revision: Dec 08, 2021

---

This program solves the first homework exercise of the numerical section in the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the general README outside of the folder of this program. It should provide details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a standalone, please check sections 1. a), 1. b) and 1. c) of the general README, before running the following commands:

`gfortran -c imethods.f90 -o imethods.o`\
`gfortran -c Integral.f90 -o Integral.o`\
`gfortran Integral.o imethods.o -o Integral.exe`

Once you have run this command successfully, you should be able to run the program.

### a) Test

The only way to test if this program is working is to actually run it. Therefore, I suggest that you try running the following command:

`./Integral.exe`

The program will greet you and prompt you for the number of intervals to be used by the 3 integration methods. Once entered (I would strongly advise you to use the suggested numbers for $`N`$), the program should display the output on screen.

If the program ran successfully, then you should see the value of the following integral calculated with 3 numerical integration methods: Rectangle Method, Trapezoid Method and Simpson's Method.

```math
\int_{-1}^{1} \sin{ \left( x + 1 \right) } dx
```

The result of the integral should be a real number close to $`1.416146`$ for each method.

If this is true, then the program has run successfully.

### b) Inputs

The program will prompt the user with 3 numbers; integers for the amount of intervals or sections used by each integration method.

### c) Outputs

If you run the program as described in section 1. a) of this README, then the output will be displayed on screen.

### d) Theory behind it

It was asked that the program computed the integral of the function $`f \left( x \right) = \sin{ \left( x + 1\right)}`$ in the interval from $`-1`$ to $`1`$ using:

- The Rectangle Method
```math
\int_{a}^{b} f \left( x \right) dx \approx \frac{\left( b - a \right)}{N} \sum_{i = 0}^{N - 1} f \left( x + i \cdot \frac{\left( b - a \right)}{N} \right)
```

- The Trapezoid Method
```math
\int_{a}^{b} f \left( x \right) dx \approx \frac{\left( b - a \right)}{2 N} \sum_{i = 0}^{N - 1} \left( f \left( x + i \cdot \frac{\left( b - a \right)}{N} \right) + f \left( x + \left( i + 1 \right) \cdot \frac{\left( b - a \right)}{N} \right) \right)
```

- The Simpson Method
```math
\int_{a}^{b} f \left( x \right) dx \approx \frac{\left( b - a \right)}{3 N} \left[ f \left( a \right) + 2 \sum_{i = 0}^{N/2 - 1} f \left( x + 2 i \cdot \frac{\left( b - a \right)}{N} \right) + 4 \sum_{j = 0}^{N/2} f \left( x + (2 j - 1) \cdot \frac{\left( b - a \right)}{N} \right) + f \left( b \right) \right]
```

Since the **Simple rule** was asked from us in the homework, then the values of $`N`$ should be 1, 2 and 3 respectively. This means:

- Rectangle method: $`N_{rectangle} = 1`$
- Trapezoid method: $`N_{trapezoid} = 2`$
- Simpson's method: $`N_{simpson} = 3`$

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin or enable the WSL environment, you should be able to compile and run the program. However, we do suggest that you have a look at section 1. a) of the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file, please follow this link to locate the project's repository: [https://gitlab.com/zronyj/tc_numerical](https://gitlab.com/zronyj/tc_numerical)

3. *What happens if I use different numbers for the intervals?* - If the numbers are integers larger than the suggested value, then there shouldn't be a problem. Actually, the program might take a little longer for large numbers (e.g. $`10^{5}`$), but the result should be closer to the real value of the integral. If the value is less than the suggested one, a decimal number, or not even a number, then the program might crash or give a result which won't make any sense.