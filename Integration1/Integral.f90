! -----------------------------------------------------------------------------------------
! This function should be defined by the user of the program
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - r: a real number
!
! Outputs:
! - funcion: a real number - the number resulting in the evaluation of sin(r + 1)
!
! -----------------------------------------------------------------------------------------
real function funcion(r)
    real :: r
    funcion = sin(r + 1.0)
    return
end function


program Integral

! -----------------------------------------------------------------------------------------
! This program computes the integral of a given function using 3 methods:
! - The Rectangle method
! - The Trapezoid method
! - The Simpson method
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - funcion: a function defined at the beginning of this program. It is imported as an
!            external function.
!
! Outputs:
! - on screen: it shows the value of integrating the function in a given interval, for
!              the Simple Method in all 3 cases. This is done as a homework example.
!
! -----------------------------------------------------------------------------------------

    ! Import the integration modules ------------------------------------------------------
    use imethods, only : int_rect, int_trap, int_simp

    implicit none

    ! Import the function defined at the beginning of the file ----------------------------
    external funcion

    ! Variables ---------------------------------------------------------------------------
    integer :: nr, nt, ns
    real :: funcion, a, b, E
    real :: Ir, It, Is

    write(*, *) '--------------------------------------------------------'
    write(*, *) '|                 Integration Methods                  |'
    write(*, *) '--------------------------------------------------------'
    write(*, *) '|                                                      |'
    write(*, *) '|  This program will evaluate the following:           |'
    write(*, *) '|                                                      |'
    write(*, *) "|               1                                      |"            
    write(*, *) "|               .-                                     |"
    write(*, *) "|               |  sin(x + 1) dx                       |"
    write(*, *) "|              -'                                      |"
    write(*, *) "|              -1                                      |"
    write(*, *) '|                                                      |'
    write(*, *) '|  Using these integration methods:                    |'
    write(*, *) '|                      1. Rectangle Method             |'
    write(*, *) '|                      2. Trapezoid Method             |'
    write(*, *) "|                      3. Simpson's Method             |"
    write(*, *) '|                                                      |'
    write(*, *) '--------------------------------------------------------'
    write(*, *) '|                                                      |'
    write(*, *) '|  Please enter the number of points for the first     |'
    write(*, *) '|  method (Simple case n = 1):                         |'
    write(*, "(' ', A)", advance='no') '|  n = '
    read(*, *) nr
    write(*, *) '|                                                      |'
    write(*, *) '|  Please enter the number of points for the second    |'
    write(*, *) '|  method (Simple case n = 2):                         |'
    write(*, "(' ', A)", advance='no') '|  n = '
    read(*, *) nt
    write(*, *) '|                                                      |'
    write(*, *) '|  Please enter the number of points for the third     |'
    write(*, *) '|  method (Simple case n = 3):                         |'
    write(*, "(' ', A)", advance='no') '|  n = '
    read(*, *) ns
    write(*, *) '|                                                      |'
    write(*, *) '|  Thank you!                                          |'
    write(*, *) '|                                                      |'
    write(*, *) '|                         Computing ...                |'
    write(*, *) '|                                                      |'
    write(*, *) '--------------------------------------------------------'
    write(*, *) ''

    ! Integration limits ------------------------------------------------------------------
    a = -1.0
    b = 1.0
    
    ! Computing the integral by passing the aforementioned values -------------------------
    call int_rect(funcion, a, b, nr, Ir)
    call int_trap(funcion, a, b, nt, It)
    call int_simp(funcion, a, b, ns, Is)

    ! Showing the results of the integrals on screen --------------------------------------
    write(*, *) '--------------------------------------------------------'
    write(*, *) '| Integration by Rectangle Method:', Ir, '   |'
    write(*, *) '--------------------------------------------------------'
    write(*, *) '|      Integration for N =', nr, 'intervals.      |'
    write(*, *) '--------------------------------------------------------'
    write(*, *) ''
    write(*, *) '--------------------------------------------------------'
    write(*, *) '| Integration by Trapezoid Method:', It, '   |'
    write(*, *) '--------------------------------------------------------'
    write(*, *) '|      Integration for N =', nt-1, 'intervals.      |'
    write(*, *) '--------------------------------------------------------'
    write(*, *) ''
    write(*, *) '--------------------------------------------------------'
    write(*, *) "| Integration by Simpson's Method:", Is, '   |'
    write(*, *) '--------------------------------------------------------'
    write(*, *) '|      Integration for N =', ns-1, 'intervals.      |'
    write(*, *) '--------------------------------------------------------'

end program Integral