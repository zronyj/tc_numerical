module imethods
    implicit none
    
    ! Establishing the subroutines that will be provided to the external program
    public :: int_rect, int_trap, int_simp

    contains

    subroutine int_rect(f, a, b, n, Ir)
    ! -------------------------------------------------------------------------------------
    ! Special subroutine to calculate the integral o a function using the Rectangle Method
    ! -------------------------------------------------------------------------------------
    !
    ! Inputs
    ! - f: the function to be integrated
    ! - a: the lower limit of the integral
    ! - b: the upper limit of the integral
    ! - n: number of rectales to construct the area under the curve
    ! Outputs
    ! - Ir: the area under the curve for function f, between a and b
    ! 
    ! -------------------------------------------------------------------------------------

        implicit none

        ! In and out variables ------------------------------------------------------------
        integer, intent(in) :: n
        real, intent(in) :: a, b
        real, intent(out) :: Ir

        ! Internal variables --------------------------------------------------------------
        integer :: i
        real :: f, delta, area
            
        ! Setting the initial area and computing the length of the intervals --------------
        delta = (b - a) / n
        area = 0

        ! Computing the area by adding the area of each rectangle under the curve ---------
        do i = 0, n - 1
            area = area + f(a + i * delta)
        end do
        Ir = area * delta

    end subroutine int_rect

    subroutine int_trap(f, a, b, m, It)
    ! -------------------------------------------------------------------------------------
    ! Special subroutine to calculate the integral o a function using the Trapezoid Method
    ! -------------------------------------------------------------------------------------
    !
    ! Inputs
    ! - f: the function to be integrated
    ! - a: the lower limit of the integral
    ! - b: the upper limit of the integral
    ! - n: number of trapezoids to construct the area under the curve
    ! Outputs
    ! - It: the area under the curve for function f, between a and b
    ! 
    ! -------------------------------------------------------------------------------------

        implicit none

        ! In and out variables ------------------------------------------------------------
        integer, intent(in) :: m
        real, intent(in) :: a, b
        real, intent(out) :: It

        ! Internal variables --------------------------------------------------------------
        integer :: i, n
        real :: f, delta, area

        ! Setting the initial area and computing the length of the intervals --------------
        n = m - 1
        delta = (b - a) / n
        area = (f(a) + f(b)) / 2

        ! Computing the area by adding the area of each trapezoid under the curve ---------
        do i = 1, n - 1
            area = area + f(a + i * delta)
        end do
        It = area * delta

    end subroutine int_trap

    subroutine int_simp(f, a, b, m, Is)
    ! -------------------------------------------------------------------------------------
    ! Special subroutine to calculate the integral o a function using the Simpson Method
    ! -------------------------------------------------------------------------------------
    !
    ! Inputs
    ! - f: the function to be integrated
    ! - a: the lower limit of the integral
    ! - b: the upper limit of the integral
    ! - n: number of parabolas to construct the area under the curve
    ! Outputs
    ! - Is: the area under the curve for function f, between a and b
    ! 
    ! -------------------------------------------------------------------------------------

        implicit none

        ! In and out variables ------------------------------------------------------------
        integer, intent(in) :: m
        real, intent(in) :: a, b
        real, intent(out) :: Is

        ! Internal variables --------------------------------------------------------------
        integer :: i, n
        real :: f, delta, area

        ! Setting the initial area and computing the length of the intervals --------------
        n = m - 1
        delta = (b - a) / n
        area = f(a) + f(b)

        ! Computing the area by adding the area under the curve of each parabola ----------
        do i = 1, n - 1
            area = area + 2**(mod(i,2) + 1) * f(a + i * delta)
        end do
        Is = area * delta/3

    end subroutine int_simp

end module imethods