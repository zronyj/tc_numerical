! -----------------------------------------------------------------------------------------
! This function defines a circle with radius 1, located with its center at 1,1
! It then evaluates if the pair of coordinates x1 and y1 falls inside of it
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - x1: coordinate in the x axis to be evaluated in the function
! - y1: coordinate in the y axis to be evaluated if it falls inside the circle
!
! Outputs:
! - in_circle: an integer - 1 if the point falls inside the circle and 0 otherwise
!
! -----------------------------------------------------------------------------------------
integer function in_circle(x1, y1)
	real :: x1, y1, raiz
	raiz = sqrt((x1 - 0.5)**2 + (y1 - 0.5)**2)
	if (raiz < 0.5) then
		in_circle = 1
	else
		in_circle = 0
	end if
	return
end function in_circle

program MonteCarlo
! -----------------------------------------------------------------------------------------
! This program computes the value of PI using the Monte Carlo method
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - in_circle: a function defined at the beginning of this program which checks if a random
!              point x, y falls inside a circle with radius of 0.5
!
! Outputs:
! - on screen: it shows the number of random points generated, the number of those points
!              falling inside the circle, the estimated value of pi, and the squared error
!              compared to the real value of pi
!
! -----------------------------------------------------------------------------------------

	implicit none

	! Import the function defined at the beginning of the file ----------------------------
	external in_circle

	! Variables ---------------------------------------------------------------------------
	integer :: in_circle, i, j, temp, inside, n
	real :: pi, error
	real, allocatable :: rx(:), ry(:)

    write(*, *) '----------------------------------------------------------------'
    write(*, *) '|                      Monte Carlo Method                      |'
    write(*, *) '----------------------------------------------------------------'
    write(*, *) '|                                                              |'
    write(*, *) '|      This program will approximate Pi, using the Monte       |'
    write(*, *) '|      Carlo Method. This means that it will generate          |'
    write(*, *) '|      random points * from 0 to 2r, and it will evaluate      |'
    write(*, *) '|      how many of them fall inside a circle of radius         |'
    write(*, *) '|      r = 1. The ratio between the inside points and the      |'
    write(*, *) '|      total, times 4, will approximate Pi.                    |'
    write(*, *) '|                                                              |'
	write(*, *) "|              ^ y                                             |"
	write(*, *) "|              |                                               |"
	write(*, *) "|              |- - - -, - ~ - ,- - - - x (2r,2r)              |"
	write(*, *) "|              |*  , '   *     / ' ,  *.                       |"
	write(*, *) "|              | ,            /*     , .                       |"
	write(*, *) "|              |,    *      r/        ,.                       |"
	write(*, *) "|              |            /   *      .                       |"
	write(*, *) "|              |   *       /           .                       |"
	write(*, *) "|              |            *          .                       |"
	write(*, *) "|              |,                     ,.                       |"
	write(*, *) "|              | ,     *        *    , .                       |"
	write(*, *) "|              |  *,               ,   .                       |"
	write(*, *) "|              |*    ' - , _ , - '     .   x                   |"
	write(*, *) "|              |--------------------------->                   |"
	write(*, *) '|                                                              |'
    write(*, *) '----------------------------------------------------------------'
    write(*, *) '|                                                              |'
    write(*, *) '|      Please enter the number of points, in powers of 10,     |'
    write(*, *) '|      (P = 10^n) to be used by the Monte Carlo method.        |'
    write(*, *) '|      The standard is to start with n = 6.                    |'
    write(*, "(' ', A)", advance='no') '|      n = '
	read(*, *) n
	write(*, *) '|      Thank you!                                              |'
    write(*, *) '|                                                              |'
    write(*, *) '|                             Computing ...                    |'
    write(*, *) '|                                                              |'


	write(*, "(' ', A64)") '----------------------------------------------------------------'
	do i = 1, n
		! Initializing the array with the hits from the function --------------------------
		inside = 0
		! Regenerating the random numbers with a new seed ---------------------------------
		temp = 10**i
		allocate(rx(temp))
		allocate(ry(temp))
		call random_seed()
		call random_number(rx)
		call random_number(ry)
		do j = 1, temp
			! Evaluate if the random point falls inside the circle and add it -------------
			inside = inside + in_circle(rx(j), ry(j))
		end do

		! Estimate the value of pi in this cycle and compute the error --------------------
		pi = 4 * real(inside) / real( temp )
		error = (3.14159265359 - pi)**2
		write(*, "(' ', A3, I7, A9, I7, A38)") 'Of ', temp, ' points, ', inside, ' points have fallen inside the circle.'
		write(*, "(' ', A5, F10.8, A19, E14.8)") 'Pi ~ ', pi, 'and the error is: ', error
		write(*, "(' ', A64)") '----------------------------------------------------------------'

		! Release memory ------------------------------------------------------------------
		deallocate(rx)
		deallocate(ry)
	end do

end program MonteCarlo