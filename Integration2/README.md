
# Integration Exercise 2

Last revision: Dec 07, 2021

---

This program solves the second homework exercise of the numerical section in the course *TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Running the program

Before doing anything with the program, please make sure to read the general README outside of the folder of this program. It should provide details on how to compile all Exercises by using a `make` command.

If that is not possible or if you wish to compile this program as a standalone, please check sections 1. a), 1. b) and 1. c) of the general README, before running the following commands:

`gfortran -o MonteCarlo.exe MonteCarlo.f90`

Once you have run this command successfully, you should be able to run the program.

### a) Test

The only way to test if this program is working is to actually run it. Therefore, I suggest that you try running the following command:

`./MonteCarlo.exe`

After some seconds, this should have displayed the output on screen. If the program ran successfully, then you should see the number of random points evaluated, the number of points inside the circle, the estimated value of $`\pi`$ and the squared error with respect to the real value. If this is true, then the program has run successfully.

### b) Inputs

This program requires the user to enter an integer $`N`$, so that the program can compute $`10^N`$ random points. It is **very important** that the number is a positive integer between 1 and 8. The usual is to enter 6.

### c) Outputs

If you run the program as described above, then the output will be displayed on screen.

### d) Theory behind it

The idea of estimating the value of $`\pi`$ using the Monte Carlo method is the following:
1. Generate a large number $`N`$ of random points in 2D: $`\left( x_{i}, y_{i} \right)`$ in a range from $`0`$ to $`h`$.
2. Check how many of those points fall into a circle with radius $`\frac{h}{2}`$, with its center at $`\left( x = \frac{h}{2}, y = \frac{h}{2} \right)`$. This can be easily done using the following function. If the random value of $`y_{i}`$ falls between the two values of $`y`$ obtanied by evaluating $`x_{i}`$ in the following function, then the random point fell inside the circle.

```math
y = \frac{h}{2} \cdot \left(1 \pm \sqrt{1 - \left( 2x - 1 \right)^2} \right)
```

3. Count how many of the random points fell in the circle.
4. Divide the aforementioned sum by the total number of points and multiply that number by $`4`$. This last multiplication is necessary, because the points are being generated for a square of sides $`h`$. Then the ratio of points falling in the circle and the square should be equal to the ratio of the area of the circle and the area of the square.

```math
\frac{A_{circle}}{A_{square}} = \frac{\pi \left( \frac{h}{2} \right)^2}{h^2} = \frac{\pi h^2}{4 h^2} = \frac{\pi}{4}
```

## 2. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin or enable the WSL environment, you should be able to compile and run the program. However, we do suggest that you have a look at section 1. a) of the general README. GNU Fortran is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file, please follow this link to locate the project's repository: [https://gitlab.com/zronyj/tc_numerical](https://gitlab.com/zronyj/tc_numerical)

3. *What happens if I input a number larger than 8?* - The real question then becomes if your computer can calculate more than $`10^8`$ random points. It will take a lot of time, a fast CPU and a lot of RAM. Consider that you are generating 2, 4 bit numbers, $`10^8+`$ times. Then estimate the amount of memory that you'll need for that. Therefore, I wouldn't try it so lightly.